empty_module
=======================

Adjust the linux kernel path in Makefile.

	make
	sudo insmod empty.ko

This module create a sysfs file in /sys/kernel for testing from user space.

	sudo su
	echo 10 > /sys/kernel/test_sysfs/value
	cat /sys/kernel/test_sysfs/value
