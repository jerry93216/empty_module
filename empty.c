#define DEBUG
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/printk.h>
#include <linux/of.h>
#include <linux/io.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/platform_device.h>
#include <asm/pgtable.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <asm/current.h>
#include <linux/sched.h>
#include <linux/highmem.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/sysfs.h>
#include <linux/fs.h>
#include <linux/kobject.h>

MODULE_DESCRIPTION("empty");
MODULE_LICENSE("GPL");

volatile int value;
/* 
* Create a sysfs file
*/

static struct class *dev_class;
struct kobject *kobj_ref;

static ssize_t sysfs_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
    pr_err("[%s] Sysfs - Read\n", __func__);
    pr_debug("[%s] current ts %p theread_info %p\n", __func__,current, current_thread_info());
    return sprintf(buf, "%d\n", value);
}

static ssize_t sysfs_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t sys_count){
	sscanf(buf,"%d",&value);
	pr_debug("[%s] value %d \n", __func__, value);
    return sys_count;
}

ssize_t name_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
	pr_err("[%s] Sysfs - Display name\n", __func__);
	return sprintf(buf, "%s\n", kobject_name(kobj));
}

struct kobj_attribute name_attr = __ATTR(name, S_IRUSR, name_show, NULL);
struct kobj_attribute value_attr = __ATTR(value, S_IRUSR|S_IWUSR, sysfs_show, sysfs_store);

struct attribute *empty_attrs[]={
	&name_attr.attr,
	&value_attr.attr,
	NULL,
};

struct attribute_group empty_group = {
	.name	= "test_group",
	.attrs	= empty_attrs,
};

static int __init empty_init(void){

    if((dev_class = class_create(THIS_MODULE,"test_class")) == NULL){
        pr_err("[%s] Cannot create the struct class\n", __func__);
        return -ENODEV;
    }

    kobj_ref = kobject_create_and_add("test_sysfs",kernel_kobj);

    /*Creating sysfs file for etx_value*/
    if(sysfs_create_files(kobj_ref, (const struct attribute **)empty_attrs)){
        pr_err("[%s] Cannot create sysfs file\n", __func__);
        goto fail;
    }

	if (sysfs_create_group(kobj_ref, &empty_group)) {
		pr_err("[%s] Cannot create sysfs group\n", __func__);
        goto fail;
	}
	
	pr_err("[%s] empty module init done. \n", __func__);
    return 0;

fail:
    kobject_put(kobj_ref);
    sysfs_remove_files(kernel_kobj, (const struct attribute **)empty_attrs);
    return -1;	
}

static void __exit  empty_exit(void){
    sysfs_remove_files(kernel_kobj, (const struct attribute **)empty_attrs);
	kobject_del(kobj_ref);
	kobject_put(kobj_ref);
    class_destroy(dev_class);
}

module_init(empty_init);
module_exit(empty_exit);
