PWD := $(shell pwd)
KVERSION := $(shell uname -r)
KERNEL_DIR = ~/linux/

MODULE_NAME = empty
obj-m := -DEXPORT_SYMTAB 
obj-m := $(MODULE_NAME).o

all:
	make -C $(KERNEL_DIR) M=$(PWD) modules
clean:
	make -C $(KERNEL_DIR) M=$(PWD) clean
